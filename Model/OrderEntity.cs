﻿using System;

namespace OrderingSystem.Model
{
    public class OrderEntity
    {
        public int OrderId { get; }
        public DateTime OrderDate { get; }
        public UserEntity User { get; }
        public AlbumEntity Album { get; }
        public string Comment { get; }

        public OrderEntity(int orderId, DateTime orderDate, string comment,
            UserEntity user, AlbumEntity album)
        {
            OrderId = orderId;
            OrderDate = orderDate;
            User = user;
            Album = album;
            Comment = comment;
        }
    }
}
