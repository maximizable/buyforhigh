﻿using System;

namespace OrderingSystem.Model
{
    public class UserEntity
    {
        public int UserId { get; }
        public string Login { get; }
        public UserEntity(int userId, string login)
        {
            if (string.IsNullOrWhiteSpace(login))
            {
                throw new ArgumentNullException(nameof(login));
            }
            UserId = userId;
            Login = login;
        }
    }
}
