﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace OrderingSystem.Model
{
    public class OrderRepository : IEntityRepository<OrderEntity>
    {
        private readonly string connectionString;
        public OrderRepository()
        {
            const string DefaultConnection = nameof(DefaultConnection);
            connectionString = ConfigurationManager.ConnectionStrings[DefaultConnection].ConnectionString;
        }

        public void Create(OrderEntity entityToCreate)
        {
            using var connection = new SqlConnection(connectionString);
            connection.Open();

            string insertQuery =
                @"INSERT INTO Orders (OrderDate, UserId, AlbumId, Comment)
                    VALUES (@orderDate, @userId, @albumId, @comment)";
            var command = new SqlCommand(insertQuery, connection);

            var orderParameters = new List<SqlParameter>()
            {
                new SqlParameter("@orderDate", entityToCreate.OrderDate.ToLocalTime()),
                new SqlParameter("@userId", entityToCreate.User.UserId),
                new SqlParameter("@albumId", entityToCreate.Album.AlbumId),
                new SqlParameter("@comment", entityToCreate.Comment)
            };
            command.Parameters.AddRange(orderParameters.ToArray());

            command.ExecuteNonQuery();
        }

        public void Delete(int orderId)
        {
            using var connection = new SqlConnection(connectionString);
            connection.Open();

            string deleteQuery =
                @"DELETE FROM Orders WHERE OrderId = @orderId";
            var command = new SqlCommand(deleteQuery, connection);

            var orderParameter = new SqlParameter("@orderId", orderId);
            command.Parameters.Add(orderParameter);

            command.ExecuteNonQuery();
        }

        public IEnumerable<OrderEntity> GetAll()
        {
            using var connection = new SqlConnection(connectionString);
            connection.Open();

            string selectAllQuery =
                @"SELECT OrderId, OrderDate, Orders.UserId, Login AS UserName, Orders.AlbumId, FullName AS AlbumName, Cost, Comment
                    FROM Orders, Users, Albums
                    WHERE Orders.UserId = Users.UserId AND Orders.AlbumId = Albums.AlbumId";

            using var dataAdapter = new SqlDataAdapter(selectAllQuery, connection);
            var dataSet = new DataSet();
            dataAdapter.Fill(dataSet);

            DataTable ordersTable = dataSet.Tables[0];
            return ordersTable
                .AsEnumerable()
                .Select(row => DataRowToOrderEntity(row))
                .ToList();
        }

        public OrderEntity GetById(int orderId)
        {
            using var connection = new SqlConnection(connectionString);
            connection.Open();

            string selectQuery =
                @"SELECT OrderId, OrderDate, Orders.UserId, Login AS UserName, Orders.AlbumId, FullName AS AlbumName, Cost, Comment
                    FROM Orders, Users, Albums
                    WHERE Orders.OrderId = @orderId AND Orders.UserId = Users.UserId AND Orders.AlbumId = Albums.AlbumId";
            var command = new SqlCommand(selectQuery, connection);
            var orderIdParameter = new SqlParameter("@orderId", orderId);
            command.Parameters.Add(orderIdParameter);

            using var dataAdapter = new SqlDataAdapter(command);
            var dataSet = new DataSet();
            dataAdapter.Fill(dataSet);

            DataTable ordersTable = dataSet.Tables[0];
            return DataRowToOrderEntity(ordersTable.Rows[0]);
        }

        public void Update(OrderEntity entityForUpdate)
        {
            using var connection = new SqlConnection(connectionString);
            connection.Open();

            string updateQuery =
                @"UPDATE Orders 
                    SET OrderDate = @orderDate, UserId = @userId, AlbumId = @albumId, Comment = @comment
                    WHERE OrderId = @orderId";
            var command = new SqlCommand(updateQuery, connection);

            var orderParameters = new List<SqlParameter>()
            {
                new SqlParameter("@orderDate", entityForUpdate.OrderDate.ToLocalTime()),
                new SqlParameter("@userId", entityForUpdate.User.UserId),
                new SqlParameter("@albumId", entityForUpdate.Album.AlbumId),
                new SqlParameter("@comment", entityForUpdate.Comment),
                new SqlParameter("@orderId", entityForUpdate.OrderId)
            };
            command.Parameters.AddRange(orderParameters.ToArray());

            command.ExecuteNonQuery();
        }

        public IEnumerable<OrderEntity> GetAllOrdersByUserId(int userId)
        {
            using var connection = new SqlConnection(connectionString);
            connection.Open();

            string selectAllQuery =
                @"SELECT OrderId, OrderDate, Orders.UserId, Login AS UserName, Orders.AlbumId, FullName AS AlbumName, Cost, Comment
                    FROM Orders, Users, Albums
                    WHERE Orders.UserId = @userId AND Orders.UserId = Users.UserId AND Orders.AlbumId = Albums.AlbumId";
            var command = new SqlCommand(selectAllQuery, connection);
            var userParameter = new SqlParameter("@userId", userId);
            command.Parameters.Add(userParameter);

            using var dataAdapter = new SqlDataAdapter(command);
            var dataSet = new DataSet();
            dataAdapter.Fill(dataSet);

            DataTable ordersTable = dataSet.Tables[0];
            return ordersTable
                .AsEnumerable()
                .Select(row => DataRowToOrderEntity(row))
                .ToList();
        }

        private OrderEntity DataRowToOrderEntity(DataRow row)
        {
            return new OrderEntity(
                row.Field<int>("OrderId"), row.Field<DateTime>("OrderDate"), row["Comment"].ToString(),
                new UserEntity(row.Field<int>("UserId"), row["UserName"].ToString()),
                new AlbumEntity(row.Field<int>("AlbumId"), row["AlbumName"].ToString(), row.Field<decimal>("Cost")));
        }
    }
}
