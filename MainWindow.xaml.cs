﻿using OrderingSystem.Model;
using System;
using System.Collections.Generic;
using System.Windows;

namespace OrderingSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var repo = new OrderRepository();
            IEnumerable<OrderEntity> orderEntities = repo.GetAllOrdersByUserId(1);

            var order = new OrderEntity(18, DateTime.Now, "testUpdate",
                new UserEntity(4, "l"),
                new AlbumEntity(5, "a", 0));
            repo.Update(order);
        }
    }
}
