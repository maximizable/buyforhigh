﻿using System;

namespace OrderingSystem.Model
{
    public class AlbumEntity
    {
        public int AlbumId { get; }
        public string AlbumName { get; }
        public decimal Cost { get; }

        public AlbumEntity(int albumId, string albumName, decimal cost)
        {
            if (string.IsNullOrWhiteSpace(albumName))
            {
                throw new ArgumentNullException(nameof(albumName));
            }
            AlbumId = albumId;
            AlbumName = albumName;
            Cost = cost;
        }
    }
}
