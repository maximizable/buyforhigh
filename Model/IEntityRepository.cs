﻿using System.Collections.Generic;

namespace OrderingSystem.Model
{
    public interface IEntityRepository<TEntity>
    {
        public IEnumerable<TEntity> GetAll();
        public TEntity GetById(int id);
        public void Create(TEntity entityToCreate);
        public void Update(TEntity entityForUpdate);
        public void Delete(int id);
    }
}
